Readme:





This readme will guide you through the EquilibriumFinder.

This repo contains:
-code files
-uml diagram
-javadoc
-EquilibriumFinder.jar (using java version 1.7)
-EquilibriumFinder.jar (using java version 1.8)
-an inputfile to be used with the EquilibriumFinder


Usage:
after downloading all the files, you can use the terminal to use the EquilibriumFinder.
for help use:
java -jar EquilibriumFinder -help
for default use:
java -jar EquilibriumFinder -input [inputFile]

if you want to use a different maximum amount of possible iterations or a different value for calculating if an equilibrium has been reached use:
java -jar EquilibriumFinder -input [inputFile] -max_diff [value] AND/OR -max_init [value]


Help:
if for some highly unlikely and unforseen reason something went wrong, please contact the creator at mr.mulder984@hotmail.com.
