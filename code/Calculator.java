/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinfmuldermycoolmavenapp.equalizer;

import java.util.HashMap;
import java.util.ArrayList;

/**
 *
 * @author mrmulder
 */
public class Calculator {

    public static void main(HashMap<String, ArrayList> code, float max_diff, int max_iter) {
        //Declare variables
        //taking values out of the hashmap from inputter
        ArrayList<String> Aa = code.get("A");
        float a = Float.parseFloat(Aa.get(1));
        ArrayList<String> Bb = code.get("B");
        float b = Float.parseFloat(Bb.get(1));
        ArrayList<String> Cc = code.get("C");
        float c = Float.parseFloat(Cc.get(1));
        ArrayList<String> Dd = code.get("D");
        float d = Float.parseFloat(Dd.get(1));
        ArrayList<String> Xx = code.get("X");
        //goodx and goodx2 are the changes in reaction 1 and reaction 2 respectively
        float goodx = 0;
        float goodx2 = 0;
        float x = Float.parseFloat(Xx.get(1));
        int i = 0;
        float dif = (float) 0.0;
        if (max_iter == 0) {
            i = 100;
        } else {
            i = max_iter;
        }
        if (max_diff == 0) {
            dif = (float) 0.0001;
        } else {
            dif = max_diff;
        }
        float compare = 0;
        int counter = 0;
        while (i > 0) {
            counter++;
            if (a < 0) {
                a = 0;
            }
            if (b < 0) {
                b = 0;
            }
            //substracting the change of a and b, which is 0 the first time, because the reaction has not happened yet.
            //adding the change to d, because d is a product.
            a -= goodx;
            b -= goodx;
            d += goodx;
            //preparing the values for the ABC formula
            //c
            float ab = a * b;
            //b
            float aabb = a + b;
            //c*x
            float abx = ab * x;
            //b*x -1
            float aabbx = -aabb * x - 1;
            //ABC formula
            float x1 = (-aabbx + (float) Math.sqrt((aabbx * aabbx) - 4 * x * abx)) / 2 * x;
            float x2 = (-aabbx - (float) Math.sqrt((aabbx * aabbx) - 4 * x * abx)) / 2 * x;

            //smallest postive x is good.
            if (x1 > 0 && x1 <= x2) {
                goodx = x1;
            } else if (x1 > x2) {
                goodx = x2;
            }
            //Declare variables
            //taking values out of the hashmap from inputter
            ArrayList<String> Ee = code.get("E");
            float e = Float.parseFloat(Ee.get(1));
            ArrayList<String> Ff = code.get("F");
            float f = Float.parseFloat(Ff.get(1));
            ArrayList<String> Gg = code.get("G");
            float g = Float.parseFloat(Gg.get(1));
            ArrayList<String> Yy = code.get("Y");
            float y = Float.parseFloat(Yy.get(1));
            c += goodx;
            c -= goodx2;
            e -= goodx2;
            f += goodx2;
            g += goodx2;

            if (c < 0) {
                c = 0;
            }
            if (e < 0) {
                e = 0;
            }
            //preparing the values for the ABC formula
            //c
            float ec = e * c;

            //e
            float eecc = e + c;
            //c*x
            float ecy = ec * y;
            //e*x
            float eeccy = -eecc * y - 1;

            //ABC formula
            float x3 = (-eeccy + (float) Math.sqrt((eeccy * eeccy) - 4 * y * ecy)) / 2 * y;
            float x4 = (-eeccy - (float) Math.sqrt((eeccy * eeccy) - 4 * y * ecy)) / 2 * y;

            //smallest postive x is good.
            if (x3 > 0 && x3 <= x4 || x4 == 0) {
                goodx2 = x3;
            } else if (x4 > 0 && x4 <= x3) {
                goodx2 = x4;
            }

            if (Math.abs(goodx2 - compare) <= dif) {
                System.out.println("A + B -> C + D");
                System.out.println(Aa.get(0) + " + " + Bb.get(0) + " -> " + Cc.get(0) + " + " + Dd.get(0));
                System.out.println("C + E -> F + G");
                System.out.println(Cc.get(0) + " + " + Ee.get(0) + " -> " + Ff.get(0) + " + " + Gg.get(0) + "\n");
                System.out.println("round" + counter + " end results:  \n" + Aa.get(0) + ":A = " + a + " " + Bb.get(0) + ":B = " + b + " "
                        + Cc.get(0) + ":C = " + c + " " + Dd.get(0) + " " + ":D = " + d + "\n" + Ee.get(0) + ":E = " + e + " "
                        + Ee.get(0) + ":E = " + e + " " + Ff.get(0) + ":F = " + f + " " + Gg.get(0) + ":G = " + g + "\n");
                System.out.println("difference between the change of this round and last round was below treshold, equilibrium reached, aborting...");
                break;
            } else {
                //compare it with itself
                compare = goodx2;
                i--;
                if (i == 0) {
                    System.out.println("A + B -> C + D");
                    System.out.println(Aa.get(0) + " + " + Bb.get(0) + " -> " + Cc.get(0) + " + " + Dd.get(0));
                    System.out.println("C + E -> F + G");
                    System.out.println(Cc.get(0) + " + " + Ee.get(0) + " -> " + Ff.get(0) + " + " + Gg.get(0) + "\n");
                    System.out.println("round" + counter + " end results:  \n" + Aa.get(0) + ":A = " + a + " " + Bb.get(0) + ":B = " + b + " "
                            + Cc.get(0) + ":C = " + c + " " + Dd.get(0) + " " + ":D = " + d + "\n" + Ee.get(0) + ":E = " + e + " "
                            + Ee.get(0) + ":E = " + e + " " + Ff.get(0) + ":F = " + f + " " + Gg.get(0) + ":G = " + g + "\n");
                    System.out.println("maximum amount of rounds reached, equilibrium not found yet, aborting...");
                }

            }
        }
    }
}
