/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinfmuldermycoolmavenapp.equalizer;

import java.util.HashMap;
import java.util.ArrayList;
import org.apache.commons.cli.*;


/**
 *
 * @author mrmulder
 */
public class Equal {
    String help;
    float max_diff;
    int max_iter;

    public static void main(String[] args) {
        Options options = new Options();
        Option help = new Option("help", false, "-help: Show program options and uses");
        Option input = new Option("input", true, "-input: The path to the inputfile");
        Option max_diff = new Option("max_diff", true, "Will change the difference treshold to the one specified on the commandline, default 0.0001");
        Option max_iter = new Option("max_iter", true, "max amount of iterations, default 100");
        options.addOption(help);
        options.addOption(max_diff);
        options.addOption(max_iter);
        options.addOption(input);

        
        // create the parser
        CommandLineParser parser = new DefaultParser();
        String s;
        float a = 0;
        int b = 0;
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("help")) {
                System.out.println("options: "+ "-help, -max_diff, -max_iter");
            }
            if (line.hasOption("max_diff")) {
                a = Float.valueOf(line.getOptionValue("max_diff"));
            }
            if (line.hasOption("max_iter")) {
                b = Integer.valueOf(line.getOptionValue("max_iter"));
            }
            if (line.hasOption("input")) {
                s = String.valueOf(line.getOptionValue("input"));
                String[] ary = s.split("");
                HashMap<String, ArrayList> code = Inputter.Input(s);
                Calculator.main(code, a, b);
            }
            
            
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }
//        System.out.println(max_diff);
//        System.out.println(max_iter);

    }
}
