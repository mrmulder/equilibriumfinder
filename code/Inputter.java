/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinfmuldermycoolmavenapp.equalizer;

import java.io.*;
import java.util.HashMap;
import java.util.ArrayList;


/**
 *
 * @author mrmulder
 */
public class Inputter {

    public static HashMap Input(String args) {
        // The name of the file to open.
        String fileName = args;

        // This will reference one line at a time
        String line = null;
        // Making a Hashmap to be used later
        HashMap<String, ArrayList> hmap = new HashMap<>();

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader
                    = new FileReader(fileName);

            // Wrapping FileReader in BufferedReader.
            BufferedReader bufferedReader
                    = new BufferedReader(fileReader);
            // Reading 1 line at a time, ignoring lines that start with #
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                // If line starts with >, splitting the line and putting the splits in the arrayList BrokenUpLine
                if (line.startsWith(">")) {
                    String[] BrokenUpLine = line.split(" ");
                    // Making an Arraylist
                    ArrayList<String> obj = new ArrayList<>();
                    // Adding Adding values to the Arraylist
                    obj.add(BrokenUpLine[2]);
                    obj.add(BrokenUpLine[4]);
                    // Making Hashmap with keys A t/m G and values Arraylist of chemical + concentration, ex. (A,(CO2, 4))
                    hmap.put(BrokenUpLine[1], obj);
                }
            }
            // Closing file.
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" + fileName + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '" + fileName + "'");
        }
        return (hmap);
    }
}
